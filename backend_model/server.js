// global imports
import express from 'express'
import mongoose from 'mongoose'

// local constants
const DEV_PORT = 8000     // port to host the development server on

// do local imports
import { signup_user_handler } from "./path_handlers/user_paths.js"
import { user_login_handler } from "./path_handlers/user_paths.js"
//import { create_book_handler, read_books_handler, update_book_handler, delete_book_handler } from "./path_handlers/book_paths.js"
//import { read_testimonials_handler, create_testimonials_handler, delete_testimonials_handler } from "./path_handlers/testimonials_paths.js"
//import { read_book_reviews_handler, create_book_review_handler, delete_book_review_handler } from "./path_handlers/reviews_paths.js"

// initializing the express app
const app = express()
/*
 express.json() function is a built-in middleware function in Express. 
 It parses incoming requests with JSON payloads and is based on body-parser
*/
app.use(express.json());

// setup your API end points

//user sign_up and login
app.post('/signup', signup_user_handler)                
app.post('/login', user_login_handler)                     
          
//adding items,price of product and getting products,getting product based on id
app.post('/adding_products',adding_user_handler) 
app.post("/price/:product_id",adding_price_handler)
app.get('/price/:product_id',price_of_product_handler)  
app.get('/getallproducts', prodcuts_handler)      
app.get("/getall_products/:product_id",product_id_handler)  

//adding reviews and rating to the product    
app.post("/reviews",add_review_handler)
app.get("/reviews/:product_id",product_review_handler)
app.post("/rating/:product_id",add_rating_handler)
app.get("/rating/:product_id",product_rating_handler)


//adding items to cart,getting cart items,updating added items,delete cart items
app.post('/add_to_cart/:product_id',add_to_cart_handler)  
app.get("/cart_items",cart_items_handler)  
app.put("/cart_items",)
app.delete("/delete_cart_product/:product_id")  

//inserting testimony,getting tesimonials
app.post('/testimonials')   
app.get('/testimonials')       


//payment method
app.post("/payment_mode/:product_id")
app.get("/payment_mode/:product_id")

//adding address
app.post("/address/:product_id")       
     

//application listening to the port
mongoose.connect("mongodb+srv://book_store:book@bookstore.2irtl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", 
  () => {
    console.log("DB Connection successful");
    app.listen(DEV_PORT, () => {
      console.log("listening to port " + DEV_PORT);
    });
});
