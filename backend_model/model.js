// global
import mongoose from "mongoose";

// creating the user schema
const user_schema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  email: String,
  password: String,
  role: String,
  addresses: [address_schema]
});

const products_schema = new mongoose.Schema({
  product_id: Number,
  image: String,
  product_price: Number,
  product_name: String,
  reviews: [reviews_schema],
});

const cart_schema = new mongoose.Schema({
  products: [products_schema],
});

const testimonials_schema = new mongoose.Schema({
  testimonial: String,
  who: UUID // ref
});

const reviews_schema = new mongoose.Schema({
    review: String, // optional 
    stars: Number, // 1, 2, 3, 4, 5, compulsory
    product: [products_schema],
    user: [user_schema],
    time: DateTime
  });
  
  const address_schema = new mongoose.Schema({
    mobileno: Number,
    street_name: String,
    House_no: String,
    Landmark: String,
    pincode: Number,
    state: String,
    country: String,
  });
  
  const payment_method_schema = new mongoose.Schema({
    method: String
  });
  
  const UserModel = mongoose.model("User", user_schema);
  const ProductModel = mongoose.model("Products", products_schema);
  const AddCartModel = mongoose.model("Cartadd", cart_schema);
  const testimonialaModel = mongoose.model("Testimonials", testimonials_schema);
  const ReviewsModel = mongoose.model("Reviews", reviews_schema);
  const AddressModel = mongoose.model("Address", address_schema);
  const payementModel = mongoose.model("Payment", payment_method_schema);

  export {
    UserModel,
    ProductModel,
    AddCartModel,
    testimonialaModel,
    ReviewsModel,
    AddressModel,
    payementModel
  
  };
  