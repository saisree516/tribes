
import './App.css';
import React, { Component } from "react";
import Header from "./components/header/header.js"
import Middlesection from './components/middle_section/middle_contianer';
import Product_types from './components/middle_section/product_types';
import Environment from './components/middle_section/environment/environment';
import Product_description from './components/middle_section/product_des/product_description';
import Product_ingredients from './components/middle_section/product_ingredients/product_ingredients';
import Testimony from './components/middle_section/Testimony';
import Explore_heading from './components/middle_section/explore_more';
import Footer from './components/footer/footer';
import { Product_descriptions } from './data';
import {Product_ingredient} from "./data";
import { Footer_page } from './data';
import { Environments } from './data';

class App extends Component {
  render() {
    return (
      <>
      <Header></Header>
      <Middlesection></Middlesection>
      <Product_types></Product_types>
      <Environment Environments={Environments}></Environment>

      <Product_description Product_descriptions={Product_descriptions}/>
      <Product_ingredients Product_ingredient={Product_ingredient}/>
      <Testimony></Testimony>
      <Explore_heading></Explore_heading>
      <Footer Footer_page={Footer_page}/>
    </>
    
    )}
}


export default App
