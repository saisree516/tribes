import '../../App.css';
import React, { Component } from "react";
import Foooter from './Foooter';

class Footer extends Component {
  render() {
    const {Footer_page}=this.props
    return (
        <div className="section">
        <div className="footer ">
          <div className="container white bgcolor">
          
          {Footer_page.map((link) => (
          <Foooter  eachlink={link}/>
          ))} 
            <div className="group" />
          </div>
        </div>
      </div>  
    )}}
export default Footer