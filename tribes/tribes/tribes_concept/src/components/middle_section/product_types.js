import '../../App.css';
import React, { Component } from "react";


class Product_types extends Component {
  render() {
    return (
        <div className="section bg">
        <div className="container">
          <h1>Products</h1>
          <h2>Let's look into this!</h2>
          <div className="col three bg nopad pointer">
            <div className="imgholder" />
            <h1 className="feature">Face brightning daily cleanser</h1>
            <p>Rs.500</p>
          </div>
          <div className="col three bg nopad pointer">
            <div className="imgholder" />
            <h1 className="feature">Face brightning kit</h1>
            <p>Rs.1214</p>
          </div>
          <div className="col three bg nopad pointer">
            <div className="imgholder" />
            <h1 className="feature">90 Day miracle hair oil</h1>
            <p>Rs.640</p>
          </div>
          <div className="group margin" />
          <div className="col three bg nopad pointer">
            <div className="imgholder" />
            <h1 className="feature">Oragnic hair cleanser</h1>
            <p>Rs.500</p>
          </div>
          <div className="col three bg nopad pointer">
            <div className="imgholder" />
            <h1 className="feature">Face glowkit</h1>
            <p>Rs.1500</p>
          </div>
          <div className="col three bg nopad pointer">
            <div className="imgholder" />
            <h1 className="feature">Anti-acene treatment kit</h1>
            <p>Rs.830</p>
          </div>
          <div className="group" />
        </div>
      </div>
                
    )}}
export default Product_types