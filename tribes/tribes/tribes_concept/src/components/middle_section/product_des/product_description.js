
import '../../../App.css';
import React, { Component } from "react";
import Products from './description';

class Product_description extends Component {
  render() {
    const {Product_descriptions}= this.props
    return (
        <div className="section">
        <div className="container description">
          <h1>Product details</h1>
          <h2>Find here</h2>
          {Product_descriptions.map((link) => (
          <Products  eachlink={link}/>
          ))}
          <div className="group margin" />
          <div className="group margin" />
        </div>
      </div>
               
    )}}
export default Product_description