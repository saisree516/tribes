
import React, { Component } from "react";
import '../../../App.css'


class Environments_des extends Component{
  render(){
    const {eachlink} = this.props
    return(
        
        <div className="col four">
        <h1 className="icon" ><img src={eachlink.image} className='image2'/></h1>
        <h1 className="service">{eachlink.heading}</h1>
        <p>{eachlink.para}</p>
      </div>
    )
  }
}

export default Environments_des
